module "bastion" {
  source                      = "github.com/terraform-community-modules/tf_aws_bastion_s3_keys"
  instance_type               = "${var.instance_type}"
  ami                         = "${var.ami_id}"
  region                      = "${var.region}"
  iam_instance_profile        = "${var.instance_profile}"
  s3_bucket_name              = "${var.s3_bucket_name}"
  vpc_id                      = "${module.vpc.vpc_id}"
  subnet_ids                  = ["${module.vpc.public_subnets}"]
  keys_update_frequency       = "5,20,35,50 * * * *"
  associate_public_ip_address = "true"
  key_name                    = "eduardo_flugel"
  additional_user_data_script = "sudo apt-get update && sudo apt-get upgrade -y"
}

# This is just a sample definition of IAM instance profile which is allowed to read-only from S3.
resource "aws_iam_instance_profile" "s3_readonly" {
  name = "s3_readonly"
  role = "${aws_iam_role.s3_readonly.name}"
}

resource "aws_iam_role" "s3_readonly" {
  name = "s3_readonly"
  path = "/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "s3_readonly_policy" {
  name = "s3_readonly-policy"
  role = "${aws_iam_role.s3_readonly.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1425916919000",
            "Effect": "Allow",
            "Action": [
                "s3:List*",
                "s3:Get*"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}



# This is an example of how to put public keys into S3 bucket and manage them in Terraform
variable "ssh_public_key_names" {
  default = ["emartinez","aramirez"]
  type    = "list"
}

resource "aws_s3_bucket" "ssh_public_keys" {
  region = "${var.region}"
  bucket = "oc-public-keys-provision"
  acl    = "private"

  policy = <<EOF
{
	"Version": "2008-10-17",
	"Id": "Policy142469412148",
	"Statement": [
		{
			"Sid": "Stmt1424694110324",
			"Effect": "Allow",
			"Principal": {
				"AWS": "arn:aws:iam::258279436410:root"
			},
			"Action": [
				"s3:List*",
				"s3:Get*"
			],
			"Resource": "arn:aws:s3:::oc-public-keys-provision"
		}
	]
}
EOF
}

resource "aws_s3_bucket_object" "ssh_public_keys" {
  bucket = "${aws_s3_bucket.ssh_public_keys.bucket}"
  key    = "${element(var.ssh_public_key_names, count.index)}.pub"

  # Make sure that you put files into correct location and name them accordingly (`public_keys/{keyname}.pub`)
  source = "public_keys/${element(var.ssh_public_key_names, count.index)}.pub"
  count  = "${length(var.ssh_public_key_names)}"

  depends_on = ["aws_s3_bucket.ssh_public_keys"]
}
