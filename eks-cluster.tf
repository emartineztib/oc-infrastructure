resource "aws_eks_cluster" "oc-cluster" {
  name     = "${var.cluster-name}"
  role_arn = "${aws_iam_role.oc-k8s-cluster.arn}"

  vpc_config {
    security_group_ids = ["${aws_security_group.oc-cluster.id}"]
    subnet_ids         =  ["${module.vpc.private_subnets}"]
  }

  depends_on = [
    "aws_iam_role_policy_attachment.oc-k8s-cluster-AmazonEKSClusterPolicy",
    "aws_iam_role_policy_attachment.oc-k8s-cluster-AmazonEKSServicePolicy",
  ]
}

