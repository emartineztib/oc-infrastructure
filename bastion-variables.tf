
variable "instance_type" {
  description = "The instance size to deploy. Defaults to t2.micro"
  default = "t2.micro"
}

variable "vpc_id" {
  description = "vpc id"
  default = "vpc-0757c8a9c6534d8a8"
}

variable "region" {
  description = "Region"
  default = "us-east-2"
}

variable "ami_id" {
  description = "AMI ID"
  default = "ami-0653e888ec96eab9b"
}
variable "public_subnet_id" {
  description = "subnet_id"
  default = ["subnet-08e50329ce23099e7","subnet-09bd4b15a3e5e3dc8","subnet-0ae12ec67bf6edea2"]
}

variable "instance_profile" {
 default = "s3_readonly"
}

variable "s3_bucket_name" {
 default = "oc-public-keys-provision"
}