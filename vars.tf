variable "cluster-name" {
  default = "open-channel-eks-cluster"
  type    = "string"
}
