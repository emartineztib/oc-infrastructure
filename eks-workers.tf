data "aws_ami" "eks-worker" {
  filter {
    name   = "name"
    values = ["amazon-eks-node-1.12-v20190329"]
  }

  most_recent = true
  owners      = ["602401143452"] # Amazon
}

# EKS currently documents this required userdata for EKS worker nodes to
# properly configure Kubernetes applications on the EC2 instance.
# We utilize a Terraform local here to simplify Base64 encoding this
# information into the AutoScaling Launch Configuration.
# More information: https://docs.aws.amazon.com/eks/latest/userguide/launch-workers.html
locals {
  demo-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.oc-cluster.endpoint}' --b64-cluster-ca '${aws_eks_cluster.oc-cluster.certificate_authority.0.data}' '${var.cluster-name}'
USERDATA
}

resource "aws_launch_configuration" "oc-cluster" {
  associate_public_ip_address = false
  iam_instance_profile        = "${aws_iam_instance_profile.oc-cluster-nodes.name}"
  image_id                    = "${data.aws_ami.eks-worker.id}"
  instance_type               = "m4.large"
  name_prefix                 = "terraform-eks-oc-cluster"
  security_groups             = ["${aws_security_group.oc-cluster-nodes.id}"]
  user_data_base64            = "${base64encode(local.demo-node-userdata)}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "oc-cluster" {
  desired_capacity     = 3
  launch_configuration = "${aws_launch_configuration.oc-cluster.id}"
  max_size             = 3
  min_size             = 2
  name                 = "terraform-eks-oc-cluster"
  vpc_zone_identifier  = ["${module.vpc.private_subnets}"]

  tag {
    key                 = "Name"
    value               = "terraform-eks-oc-cluster"
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${var.cluster-name}"
    value               = "owned"
    propagate_at_launch = true
  }
}
