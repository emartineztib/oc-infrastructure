module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "1.66.0"
  name = "vpc-module-oc-k8s-test"
  cidr = "10.0.0.0/16"

  azs             = ["${slice(data.aws_availability_zones.available.names, 0, 3)}"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = false

  tags = "${
    map(
     "Name", "terraform-eks-oc-cluster-nodes",
     "kubernetes.io/cluster/${var.cluster-name}", "shared",
    )
  }"
  private_subnet_tags = "${
    map(
     "kubernetes.io/role/internal-elb", "1"
    )
  }"
  public_subnet_tags = "${
    map(
     "kubernetes.io/role/elb", "1"
    )
  }"
}

